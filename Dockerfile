FROM tomcat

RUN apt-get update -y
RUN apt-get install -f
RUN apt-get install git -y
RUN apt-get install nodejs -y
RUN apt-get install npm -y
RUN apt-get clean && rm -rf /var/lib/apt/lists/*  

ADD /target/project-1.0-SNAPSHOT.war /usr/local/tomcat/webapps/

EXPOSE 8080