#!/bin/bash
echo "Adding apt-keys"
wget -q -O - https://pkg.jenkins.io/debian/jenkins-ci.org.key | sudo apt-key add -
echo deb http://pkg.jenkins.io/debian-stable binary/ | sudo tee /etc/apt/sources.list.d/jenkins.list

echo "Updating apt-get"
sudo apt-get -qq update

echo "Installing default-java"
sudo apt-get -y install default-jre > /dev/null 2>&1
sudo apt-get -y install default-jdk > /dev/null 2>&1

echo "Installing git"
sudo apt-get -y install git > /dev/null 2>&1

echo "Installing git-ftp"
sudo apt-get -y install git-ftp > /dev/null 2>&1

echo "Installing maven"
sudo apt-get -y install maven > /dev/null 2>&1

echo "Installing docker"
sudo apt-get -y install docker.io > /dev/null 2>&1

echo "Installing node"
#https://github.com/nodesource/distributions/blob/master/README.md
curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash -
sudo apt-get install -y nodejs
#

#echo "Installing yarn"
#curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
#echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
#sudo apt update
#sudo apt install yarn
#sudo yarn --version

echo "Installing jenkins"
sudo apt-get -y --allow-unauthenticated install jenkins > /dev/null 2>&1
if [ -f /var/lib/jenkins/secrets/initialAdminPassword ]; then
              sudo cat /var/lib/jenkins/secrets/initialAdminPassword
            fi
sudo service jenkins start

sleep 1m

sudo systemctl start docker > /dev/null 2>&1
sudo systemctl enable docker > /dev/null 2>&1
sudo usermod -a -G docker jenkins

echo "Jenkins admin password"
JENKINSPASS=$(sudo cat /var/lib/jenkins/secrets/initialAdminPassword)
echo $JENKINSPASS