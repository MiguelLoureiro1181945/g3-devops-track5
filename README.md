# Class Assignment Group Projet Report - Task 5


1.Analysis
------------

Using **Jenkins** create a simple pipeline for a Maven project to have:

local smoke test of App with Docker containers; publish docker images; manually execute App in cloud containers

## STEPS:

1. Virtual machines

   Control host

Create a _Vagrantfile and place the following script:

```
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/bionic64"

# This provision is common for all VMs
  config.vm.provision "shell", inline: <<-SHELL
    sudo apt-get update -y
    sudo apt-get install iputils-ping -y
    sudo apt-get install -y avahi-daemon libnss-mdns
    sudo apt-get install -y unzip
    sudo apt-get install openjdk-8-jdk-headless -y
    # ifconfig
  SHELL

# Configurations specific to the control VM
      config.vm.define "control" do |control|
          control.vm.host_name = "control"
          control.vm.network "private_network", ip:"192.168.100.10"
          control.vm.network "forwarded_port", guest: 22, host: 2222, auto_correct: true
          control.vm.network "forwarded_port", guest: 8080, host: 8081, auto_correct: true
          control.vm.provision "shell", path: "jenkins.sh"
          control.vm.provider :virtualbox do |vb|
              vb.customize ["modifyvm", :id, "--memory", "1024"]
              vb.customize ["modifyvm", :id, "--cpus", "1"]
          end
		
      end
end
```

Also create a _jenkin.sh_ bash file to install **_Jenkins_** with the following script:

```
#!/bin/bash
echo "Installing jenkins"

    if [ ! -d /var/lib/jenkins ]; then
        wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
        if [ ! -f /etc/apt/sources.list.d/jenkins.list ]; then
            echo "deb https://pkg.jenkins.io/debian-stable binary/" >> /etc/apt/sources.list.d/jenkins.list
        fi
        apt-get update
        apt-get install jenkins -y
        if [ -f /var/lib/jenkins/secrets/initialAdminPassword ]; then
          cat /var/lib/jenkins/secrets/initialAdminPassword
        fi
    fi
```


## Create the control host VM

`vagrant up`



![img](https://i.imgur.com/1IrUY7T.png)


First admin password

![img](https://i.imgur.com/H5XtcAb.png)


![img](https://i.imgur.com/mrHTkq3.png)

First start and install necessary plugins:

![img](https://i.imgur.com/CTuPnjj.png)

![img](https://i.imgur.com/CAd2NOi.png)

![img](https://i.imgur.com/keDPStC.png)

![img](https://i.imgur.com/aueAFmN.png)

![img](https://i.imgur.com/GOtN07p.png)

![img](https://i.imgur.com/hRdzjlk.png)

Jenkins running

![img]()

![img](https://i.imgur.com/hRdzjlk.png)

Create a _jenkinsfile_

![img](https://i.imgur.com/uQAkhLg.png)

Edit _jenkinsfile_ and add the next script:

```
pipeline {
    agent any

    stages {

        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: 'mloureiro1181945-g3-devops-track5-credentials', url: 'https://bitbucket.org/RicardoNogueira0025/switch_2020_g3_devops/src/master/'

            }
        }
    }
}
```


![img](https://i.imgur.com/mfbmFh0.png)

![img]()

![img]()

![img]()

![img]()

![img]()

![img]()

---------------------------------------------

XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
_________________________________________


2.Setup
----------



*Note: _Jenkins_ is installed. Please open http://localhost:8080 to access Jenkins and execute any necessary additional setup step.

3.Tasks description
------------

1.



![img](https://i.imgur.com/mrHTkq3.png)

First start and install necessary plugins:

![img](https://i.imgur.com/CTuPnjj.png)

![img](https://i.imgur.com/CAd2NOi.png)

![img](https://i.imgur.com/keDPStC.png)

![img](https://i.imgur.com/aueAFmN.png)

![img](https://i.imgur.com/GOtN07p.png)

![img](https://i.imgur.com/hRdzjlk.png)

Jenkins running

![img]()

![img](https://i.imgur.com/hRdzjlk.png)

Create a _jenkinsfile_

![img](https://i.imgur.com/uQAkhLg.png)

Edit _jenkinsfile_ and add the next script:

```
pipeline {
    agent any

    stages {

        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: 'mloureiro1181945-g3-devops-track5-credentials', url: 'https://bitbucket.org/RicardoNogueira0025/switch_2020_g3_devops/src/master/'

            }
        }
    }
}
```


![img](https://i.imgur.com/mfbmFh0.png)

![img](https://i.imgur.com/mSQOAao.png)

![img](https://i.imgur.com/f9tWPSc.png)

![img](https://i.imgur.com/f9tWPSc.png)

![img](https://i.imgur.com/q5NWByU.png)

![img](https://i.imgur.com/CCT7s8N.png)

![img](https://i.imgur.com/J7E5FOG.png)

## Build#4 failed!

- Reasons: missing **Pipeline NPM Integration** plugin

- Correction: install plugin

![img](https://i.imgur.com/VuQYdcO.png)

Plugin installed. Now run again.

![img]()

![img]()

![img]()

![img](https://i.imgur.com/c2juvZz.png)

![img](https://i.imgur.com/rZVYTbn.png)

![img](https://i.imgur.com/yGQUCxb.png)

Edit _Jenkinsfile_ with the next script:



![img]()

![img]()

Made several changes (moving to a separate branch and testing stages)

![img](https://i.imgur.com/Szt75q7.png)

![img](https://i.imgur.com/tnohE8r.png)

Made a change in VM to increase memory and add node and npm install

Edit the _**Vagrantfile**_ and add the next script:

```
   ...
   control.vm.provider "virtualbox" do |vb|
     vb.memory = "2048"
  end
  
  ...


#   Installing node and npm
    if [ ! -f /usr/bin/nodejs ]; then
            curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
            apt-get install -y nodejs
            sudo apt-get install -y build-essential
            npm install -g grunt-cli gulp-cli bower
        fi
#   Installing Jenkins

```

![img](https://i.imgur.com/0sqoNmE.png)

Failed again. Wrong path to archive.

![img](https://i.imgur.com/jCOQNmL.png)

## Build #14 successful

- Still only for Backend, going to implement to Frontend
  After Frontend has is dependencies installed and a compiled archive must change **Archive** stage to also archiving the frontend build.

## Frontend assemble and compile

- Install frontend dependencies

Edit the _**Jenkinsfile**_ in the frontend build stage to have the following:

- Test this solution from [here](https://answers.netlify.com/t/new-ci-true-build-configuration-treating-warnings-as-errors-because-process-env-ci-true/14434)

`https://answers.netlify.com/t/new-ci-true-build-configuration-treating-warnings-as-errors-because-process-env-ci-true/14434`


```
CI= npm run build
``` 

![img](https://i.imgur.com/DWif2cq.png)

## Build #15,#16,#17 and #18 failed!!!

- Reasons: npm install takes too long to install and pipeline is aborted

- Solution : after seeking for possible solution I came across with this solution: [here](https://stackoverflow.com/questions/31241503/execute-npm-install-in-jenkins-only-if-the-package-json-has-changes)

```
stage('Install Frontend dependencies') {
             when {
                changeset "package.json"
             }
                     steps {
                        echo 'Installing dependencies...'
                        dir('DEVOPS/Track 5/frontend'){
                            script {
                                if(isUnix()) {
                                    sh 'npm install'
                                }
                                else {
                                    bat 'npm install'
                                }
                            }
                        }
                    }
        }
```

![img]()

![img]()

![img]()

![img]()

- Compile frontend

![img]()

## Build #99 failed again!!!

- Need to install npm react-scripts 

![img](https://i.imgur.com/mibQpWh.png)



![img]()

![img]()

![img]()





---------------------------------------------

XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
_________________________________________


2.Setup
----------



*Note: _Jenkins_ is installed. Please open http://localhost:8080 to access Jenkins and execute any necessary additional setup step.

3.Tasks description
------------

1. 


4.Implementation
----------

Define pipeline as code (in a language called Groovy, the same used by Gradle).

By convention the script of the pipeline is stored on files named _**Jenkinsfile**_.

This file is usually located in the root folder of the source code repository.

4.1 . Create pipeline
------------------------

1. Pipeline configuration:

Go to **Configure_ and define the repository and set the path for the _Jenkinsfile_.

-Note: the path if the _Jenkinsfile_ is relative to the root of the repository, in my case is ``` ```, so in my case is on folder ``` ```.



-Note: since my repository is _private_ a credential must be supplied to have access.

Maven background
------------------

### Running Maven Tools

#### Maven Phases

Although hardly a comprehensive list, these are the most common default lifecycle phases executed.

- validate: validate the project is correct and all necessary information is available

- compile: compile the source code of the project

- test: test the compiled source code using a suitable unit testing framework. These tests should not require the code be packaged or deployed package: take the compiled code and package it in its distributable format, such as a JAR.

- integration-test: process and deploy the package if necessary into an environment where integration tests can be run

- verify: run any checks to verify the package is valid and meets quality criteria

- install: install the package into the local repository, for use as a dependency in other projects locally

- deploy: done in an integration or release environment, copies the final package to the remote repository for sharing with other developers and projects.

There are two other Maven lifecycles of note beyond the default list above. They are

- clean: cleans up artifacts created by prior builds

- site: generates site documentation for this project

Phases are actually mapped to underlying goals. The specific goals executed per phase is dependant upon the packaging type of the project. For example, package executes jar:jar if the project type is a JAR, and war:war if the project type is - you guessed it - a WAR.

An interesting thing to note is that phases and goals may be executed in sequence.

mvn clean dependency:copy-dependencies package
This command will clean the project, copy dependencies, and package the project (executing all phases up to package, of course).

### Generating the Site

`mvn site`

This phase generates a site based upon information on the project's pom. You can look at the documentation generated under target/site.

________________________________

Steps
----------------

1. Create a locally hosted control, i.e., a locally hosted Jenkins instance using a Virtual Machine.


- Pre-requisites: must have installed **Virtual Box** and **Vagrant** into our machine.

### Vagrantfile

The Vagrantfile contains the parameters of the virtual machine which should be created.

Copy the following script into your _Vagrantfile_. The script will create a virtual machine which has 1 CPU and 1 GB memory, and runs an Ubuntu Xenial 64 operating system.

- Note: _**jenkins.sh**_ bash file contains the installation commands of Java, and Jenkins

### jenkins.sh

Copy the following script into your _jenkins.sh_ bash file.

```


```

### .groovy files

## Jenkins instance is finally set.


Now it’s time to do some magic. Open your Vagrantfile directory in terminal and type: `

vagrant up`

Wait for a while.

When the machine starts up, open `http://localhost:80801 in your browser. You will see **Jenkins** main page.



6. Sources


### aaa
[here]() ``

### aaa
[here]() ``

### aaa
[here]() ``

### aaa
[here]() ``

### aaa
[here]() ``

### aaa
[here]() ``

### aaa
[here](https://stackoverflow.com/questions/47415732/best-way-to-install-docker-on-vagrant) `https://stackoverflow.com/questions/47415732/best-way-to-install-docker-on-vagrant`

### Building Docker Images to Docker Hub Using Jenkins Pipelines
[here](https://dzone.com/articles/building-docker-images-to-docker-hub-using-jenkins) `https://dzone.com/articles/building-docker-images-to-docker-hub-using-jenkins`



7.Finish the class assignment
-------

* Add tag `g3-task5` to the repository